INTRODUCTION
------------

This simple module allows anonymous users or users without permissions
to submit basic node content to your site before login
in/registering. (Currently, by "basic node contect" we mean title and
body only.)

When the user finally register/log in *and got to the node creation
page*, then the node form will be pre-filled with previously input data.

NB: this module neither redirect to nor enforce the user to go to the
node creation page. It's up to your site to design a
login/registration worlklow using other Drupal means, that lead the
user to the right content creatuon page.



REQUIREMENTS
------------

None



HOW IT WORKS
------------

The module makes available pseudo-node creation forms on
`node/addlf/*TYPE*` (notice the "lf") where *TYPE* is a registered
content type. When you go to `node/addfl/*TYPE*`, two things may
happen:

* If you have the "create *type* content" permission, then you are
transparently redirected to `node/add/*TYPE*` and the basic node
fields are pre-filled with previously input data.

* If you **don't** have have the "create *type* content" permission,
then a simple form with title and body fields will be presented. After
submiting this form, the fields are saved in the user session and the
user is redirected to another configurable URL (generally with
explanation of how to register/log in, link to to registration pages,
etc.). After the user log in **and got to the node create page for
contention type *type* **, then the data input previuosly (if any) are
user to pre-fill the node title/body. After that, the session data
used is cleared to prevent it from being used again.

The module also provides a "Login First" block, that you can use to
place the form in a block. Just don't forget to enter the block
settings and setup the right content type. Currently, only one
content type can be specified.


INSTALLATION
------------

* Install module as usual (see
  https://www.drupal.org/documentation/install/modules-themes/modules-5-6)

* Goto Site configuration >> Login First (admin/settings/loginfirst)
  to setup the module



SEE ALSO
--------

These are similar modules:

* https://www.drupal.org/project/sup
* https://www.drupal.org/project/inline_registration
